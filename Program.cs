﻿//Ejemplo de arreglo

using System;

int[] array = { 1, 56, 89, 34, 12, 8 };

//Buscar el número 89

int elementoAbuscar = 89;

for (int i = 0; i < array.Length; i++)
{
    int elemento = array[i];
    Console.WriteLine("[" + i + "]");
    if (elemento == elementoAbuscar)
    {
        Console.WriteLine("El número " + elemento + " se encuentra en la posición número: " + i);
        break;
    }
}

Console.WriteLine();

//Ordenar números de menor a mayor
int auxiliar = 0;

for (int i = 0; i < array.Length; i++)
{
    for (int j = 0; j < array.Length - 1; j++)
    {
        if (array[j] > array[j + 1])
        {
            auxiliar = array[j];
            array[j] = array[j + 1];
            array[j + 1] = auxiliar;
        }
    }
}
int k = 0;
Console.WriteLine("Números ordenados de menor a mayor: ");
while (k < array.Length)
{
    Console.Write("[ " + array[k] + " ]");
    k++;
    if (k < array.Length)
        Console.Write(" => ");
}
Console.WriteLine();

Console.WriteLine();

//Ordenar números de mayor a menor
int aux = 0;

for (int i = 0; i < array.Length; i++)
{
    for (int j = 0; j < array.Length - 1; j++)
    {
        if (array[j] < array[j + 1])
        {
            aux = array[j];
            array[j] = array[j + 1];
            array[j + 1] = aux;
        }
    }
}
int t = 0;
Console.WriteLine("Números ordenados de mayor a menor: ");
while (t < array.Length)
{
    Console.Write("[ " + array[t] + " ]");
    t++;
    if (t < array.Length)
        Console.Write(" <= ");
}
Console.WriteLine();

Console.WriteLine();

//Eliminar un número del arreglo
int elementoAeliminar = 1;

Console.WriteLine("Eliminar un número");
for (int i = 0; i < array.Length; i++)
{
    if (array[i] != elementoAeliminar)
    {
        Console.Write("[ " + array[i] + " ]");
    }
    if (i + 1 < array.Length - 1)
        Console.Write(" => ");
}
Console.WriteLine();